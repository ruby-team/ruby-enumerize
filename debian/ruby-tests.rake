require 'gem2deb/rake/testtask'
#require 'gem2deb/rake/spectask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << 'test'
  t.test_files = FileList.new('test/*_test.rb') do |fl|
    fl.exclude(/formtastic|simple_form/)
  end
end

#Gem2Deb::Rake::RSpecTask.new do |spec|
#  spec.pattern = './spec/**/*_spec.rb'
#end
#
#task default: [:spec, :test]
