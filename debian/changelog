ruby-enumerize (2.8.1-1) unstable; urgency=medium

  [ Daniel Leidert ]
  * New upstream version 2.4.0

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Cédric Boutillier ]
  * New upstream version 2.8.1
  * Remove X?-Ruby-Versions: from d/control
  * Drop psych_allow_class.patch, not needed anymore
  * Refresh disable_some_tests.patch

 -- Cédric Boutillier <boutil@debian.org>  Fri, 19 Jul 2024 13:37:10 +0200

ruby-enumerize (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0
    + fixes minitest warning in tests
  * Refresh patch skipping some tests
  * Add patch listing explicitly classes assumed to be safely read from YAML
    in tests, as need by Psych starting from ruby3.1 (Closes: #1019620)
  * Bump Standards Version to 4.6.1 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Thu, 13 Oct 2022 23:38:15 +0200

ruby-enumerize (2.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Add Rails 6.1 support.
  * d/control (Standards-Version): Bump to 4.6.0.
  * d/copyright: Add Upstream-Contact field.
    (Copyright): Add team.
  * d/rules: Install upstream changelog.
  * d/patches/disable_some_tests.patch: Adjust patch.
    - Remove test failing with Ruby 3.0 (upstream issue #383).
  * d/upstream/metadata: Update Changelog URL.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 29 Nov 2021 04:15:23 +0100

ruby-enumerize (2.3.1-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package
  * New upstream version 2.3.1
  * Refresh upstream metadata
  * Use gem install layout
  * Standards-Version bumped to 4.5.0 (no changes needed)
  * Debhelper compatibility level switched to 13
  * Explicitly don't require root in debian/rules
  * Drop explicit dependency on ruby interpreter
  * Refresh patch

 -- Cédric Boutillier <boutil@debian.org>  Wed, 09 Sep 2020 22:54:51 +0200

ruby-enumerize (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2
    Fixes failing tests with news ruby-activesupport (Closes: #895116)
  * Use salsa.debian.org in Vcs-* fields
  * Bump Standards-Version to 4.1.4 (no changes needed)
  * Bump debhelper compatibility level to 11
  * Update watch file to gemwatch.debian.net
  * Use https in copyright format URL

 -- Cédric Boutillier <boutil@debian.org>  Fri, 22 Jun 2018 00:34:08 +0200

ruby-enumerize (1.1.1-1) unstable; urgency=medium

  * Imported Upstream version 1.1.1
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)
  * Drop no_test_value.patch, merged upstream
  * Build depend on ruby-sequel

 -- Cédric Boutillier <boutil@debian.org>  Wed, 06 Jul 2016 22:46:27 +0200

ruby-enumerize (1.0.0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0
  * add no_test_value.patch to rename variable in test to avoid conflict with
    minitest method (Closes: #794170)
  * Refresh metadata in debian/control

 -- Cédric Boutillier <boutil@debian.org>  Wed, 12 Aug 2015 14:19:59 +0200

ruby-enumerize (0.11.0-1) unstable; urgency=medium

  * Initial release (Closes: #711985)
    - support for the simple_form and formtastic gems is currently

 -- Cédric Boutillier <boutil@debian.org>  Thu, 23 Apr 2015 16:27:42 +0200
